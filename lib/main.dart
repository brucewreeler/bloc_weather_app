import 'package:bloc_weather_app/struct/weather.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: WeatherPage(),
    );
  }
}

class WeatherPage extends StatefulWidget {
  WeatherPage({Key key}) : super(key: key);

  _WeatherPageState createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  final weatherBloc = WeatherBloc();

  Future<bool> _onBackPressed(){
    return showDialog(
      context: context,
      builder: (context)=>AlertDialog(
        title: Text("Do you really want to exit the app?"),
        actions: <Widget>[
          FlatButton(
            child: Text("No"),
            onPressed: ()=>Navigator.pop(context,false),
          ),
          FlatButton(
            child: Text("Yes"),
            onPressed: ()=> Navigator.pop(context,true),
          )
        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
          child: Scaffold(
          appBar: AppBar(
            title: Text("Weather App"),
          ),
          body: BlocProvider(
            bloc: weatherBloc,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 16),
              alignment: Alignment.center,
              child: BlocListener(
                bloc: weatherBloc,
                listener: (context, WeatherState state) {
                  if (state is WeatherLoaded) {
                    print("Loaded: ${state.weather.cityName}");
                  }
                },
                child: BlocBuilder(
                  bloc: weatherBloc,
                  builder: (BuildContext context, WeatherState state) {
                    if (state is WeatherInitial) {
                      return buildInitialInput();
                    } else if (state is WeatherLoading) {
                      return buildLoading();
                    } else if (state is WeatherLoaded) {
                      return buildColumnWithData(state.weather);
                    } else if (state is ToMain) {
                      return buildInitialInput();
                    }
                  },
                ),
              ),
            ),
          )),
    );
  }

  
  Column buildColumnWithData(Weather weather) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          weather.cityName,
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.w700,
          ),
        ),
        Text(
          "${weather.temperature.toStringAsFixed(1)} °C",
          style: TextStyle(fontSize: 40),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(
              flex: 1,
            ),
            Icon(
              weather.icon,
              color: weather.color,
              size: 80,
            ),
            Spacer(
              flex: 1,
            ),
            Text(
              weather.desc,
              style: TextStyle(fontSize: 30),
            ),
            Spacer(
              flex: 1,
            )
          ],
        ),
        Text("Local Time: ${weather.localTime}",
        style: TextStyle(fontSize: 20),),
        ReturnButton(),
        //CityInputField()
      ],
    );
  }

  @override
  void dispose() {
    weatherBloc.dispose();
    super.dispose();
  }

  Widget buildInitialInput() {
    return Center(child: CityInputField()
        //child: Column(
        //children: <Widget>[
        //Spacer(flex: 2,),
        //CityInputField(),
        //Spacer(flex: 3,),
        //CoordButton(),
        //Spacer(flex: 2,),
        //],
        //),

        );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}

class CityInputField extends StatefulWidget {
  const CityInputField({
    Key key,
  }) : super(key: key);

  @override
  _CityInputFieldState createState() => _CityInputFieldState();
}

class _CityInputFieldState extends State<CityInputField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: TextField(
        onSubmitted: submitCityName,
        textInputAction: TextInputAction.search,
        decoration: InputDecoration(
          hintText: "Enter a city",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
          suffixIcon: Icon(Icons.search),
        ),
      ),
    );
  }

  void submitCityName(String cityName) {
    final weatherBloc = BlocProvider.of<WeatherBloc>(context);
    weatherBloc.dispatch(GetWeather(cityName));
  }
}

class ReturnButton extends StatefulWidget {
  const ReturnButton({
    Key key,
  }) : super(key: key);

  @override
  _ReturnButtonState createState() => _ReturnButtonState();
}

class _ReturnButtonState extends State<ReturnButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50),
        child: RaisedButton(
          child: Text("Return to Menu"),
          onPressed: () {
            returnToMain();
          },
        ));
  }

  void returnToMain() {
    final weatherBloc = BlocProvider.of<WeatherBloc>(context);
    weatherBloc.dispatch(ReturnToMain());
  }
}

//Struggled getting the get location to work to find weather by phone coords
class CoordButton extends StatefulWidget {
  const CoordButton({
    Key key,
  }) : super(key: key);

  @override
  _CoordButtonState createState() => _CoordButtonState();
}

class _CoordButtonState extends State<CoordButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50),
        child: RaisedButton(
          onPressed: () {
            coordGetWeather();
          },
          child: Text("Get local weather"),
        ));
  }

  void coordGetWeather() {
    final weatherBloc = BlocProvider.of<WeatherBloc>(context);
    weatherBloc.dispatch(GetCoordWeather());
  }
}
