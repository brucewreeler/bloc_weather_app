import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:bloc_weather_app/struct/weather.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './bloc.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';


class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  @override
  WeatherState get initialState => WeatherInitial();

  @override
  Stream<WeatherState> mapEventToState(
    WeatherEvent event,
  ) async* {
    if (event is GetWeather) {
      yield WeatherLoading();
      final weather = await _fetchWeatherFromApi(event.cityName);
      yield WeatherLoaded(weather);
    }
    if (event is GetCoordWeather) {
      yield WeatherLoading();
      final weather = await _fetchCoordWeatherFromApi();
      yield WeatherLoaded(weather);
    }
    if (event is ReturnToMain) {
      yield ToMain();
    }
  }

  Future<Weather> _fetchWeatherFromApi(String cityName) async {
    final res = await http.get(
        'http://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=4a8bfd86ea2c4f89a39d4e24b63ea439&ttl=3000');
    if (res.statusCode == 200) {
      final resJson = WeatherJson.fromJson(json.decode(res.body));
      print(resJson.desc);
      print(resJson.type);
      IconData icon;
      String description;
      Color color;
      if (resJson.desc != null)
        description = resJson.desc;
      else
        description = "";

      if (resJson.type == "Clear") {
        icon = Icons.wb_sunny;
        color = Color.fromARGB(255, 200, 180, 30);
      } else if (resJson.type == "Clouds") {
        icon = Icons.cloud;
        color = Color.fromARGB(255, 160, 160, 160);
      } else if (resJson.type == "Rain") {
        icon = Icons
            .cloud_download; //Out of time used icons, couldn't find rain :)
        color = Color.fromARGB(255, 130, 130, 230);
      } else if (resJson.type == "Thunderstorm") {
        icon = Icons.cloud_download;
        color = Color.fromARGB(255, 200, 180, 30);
      } else if (resJson.type == "Snow") {
        icon = Icons.ac_unit;
        color = Color.fromARGB(255, 30, 30, 230);
      } else {
        icon = Icons.device_unknown;
        color = Color.fromARGB(255, 255, 255, 255);
      }

      var date =
          new DateTime.fromMillisecondsSinceEpoch(resJson.localTime * 1000);

      String formattedTime = DateFormat('kk:mm:ss').format(date);
      print(date);
      return Weather(
          cityName: resJson.name,
          temperature: resJson.temp - 273.15,
          desc: description,
          icon: icon,
          color: color,
          localTime: formattedTime);
    } else {
      final resJson = GetError.fromJson(json.decode(res.body));
      print(resJson.message);
      String newMes;
      if (resJson.message == "city not found")
        newMes = "City not found";
      else
        newMes = resJson.message;
      return Weather(
          cityName: newMes,
          temperature: 0,
          desc: "",
          icon: Icons.cancel,
          color: Color.fromARGB(255, 255, 255, 255),
          localTime: "");
    }
  }

  Future<Weather> _fetchCoordWeatherFromApi() async {
    Map<String, double> _userLocation;
    bool permission;
    try {
      var _location = new Location();
      permission = await _location.hasPermission();
      print(permission);
      _userLocation = await _location.getLocation();
    } on PlatformException catch (e) {
      return Weather(
        cityName: "Error Getting Coords",
        temperature: 0,
      );
    }
    final res = await http.get(
        'http://api.openweathermap.org/data/2.5/weather?lat=${_userLocation["latitude"].toString()}&lon=${_userLocation["longitude"].toString()}&appid=4a8bfd86ea2c4f89a39d4e24b63ea439&ttl=3000');
    if (res.statusCode == 200) {
      final resJson = WeatherJson.fromJson(json.decode(res.body));
      print(resJson.temp);
      return Weather(
        cityName: resJson.name,
        temperature: resJson.temp - 273.15,
      );
    } else {
      final resJson = GetError.fromJson(json.decode(res.body));
      print(resJson.message);
      String newMes;
      if (resJson.message == "city not found")
        newMes = "City not found";
      else
        newMes = resJson.message;
      return Weather(
        cityName: newMes,
        temperature: 0,
      );
    }
  }
}

class WeatherJson {
  final String base;
  final double temp;
  final String name;
  final String type;
  final String desc;
  final int localTime;

  WeatherJson(
      {this.base, this.temp, this.name, this.type, this.desc, this.localTime});

  factory WeatherJson.fromJson(Map<String, dynamic> json) {
    return WeatherJson(
        base: json['base'],
        temp: json['main']['temp'],
        name: json['name'],
        type: json["weather"][0]["main"],
        desc: json["weather"][0]["description"],
        localTime: json["dt"]);
  }
}

class GetError {
  final String message;

  GetError({this.message});

  factory GetError.fromJson(Map<String, dynamic> json) {
    return GetError(
      message: json['message'],
    );
  }
}
