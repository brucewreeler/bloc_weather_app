import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class Weather extends Equatable {
  final String cityName;
  final double temperature;
  final double lat;
  final double long;
  final IconData icon;
  final String desc;
  final Color color;
  final String localTime;


  Weather({
    this.cityName,
    this.temperature,
    this.lat,
    this.long,
    this.icon,
    this.desc,
    this.color,
    this.localTime
  }):super ([cityName,temperature,lat,long,icon]);
}